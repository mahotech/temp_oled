#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_AMG88xx.h>
#include <Adafruit_MLX90614.h>
#include <PubSubClient.h>
#include "Maho.h"
#include <DoubleResetDetector.h>
#include <EEPROM.h>
// Number of seconds after reset during which a 
// subseqent reset will be considered a double reset.
#define DRD_TIMEOUT 5

// RTC Memory Address for the DoubleResetDetector to use
#define DRD_ADDRESS 0

int integer_address = 0;
int decimal_address = 1;

Maho m;
WiFiClient espClient;
PubSubClient client(espClient);
DoubleResetDetector drd(DRD_TIMEOUT, DRD_ADDRESS);

String mqttCh_c2s = "/m/c/";
String mqttCh_s2c = "/m/s/";
String mqttCh_LW = "/m/lw/"; 
const int mqttMsgSize = 2048;
int mqttLWQoS = 1;
int mqttLWRetain = 1;
int mqttClrSession = 1;
char buff[mqttMsgSize];
String mqttServer = "211.21.63.126"; 
int mqttPort = 1883;
String mqttUsr = "maho";  // maho
String mqttPwd = "tech";  // tech

int seconds = 0;

#define OLED_Address 0x3C

Adafruit_SSD1306 oled(-1);

bool isLightUp = false;

// Set Up text on OLED screen
const char* init_display = "Init Mode";
const char* linking = "Connecting to MQTT";
const char* internet_status = "Internet ";

//Adafruit_AMG88xx amg;
//float pixels[AMG88xx_PIXEL_ARRAY_SIZE];

Adafruit_MLX90614 mlx = Adafruit_MLX90614();

void setup()   {                
   Serial.begin(9600);
   
   EEPROM.begin(512);
    
 
   pinMode(LED_BUILTIN, OUTPUT);
   
  
  if (drd.detectDoubleReset()) {
    Serial.println("Double Reset Detected");
    digitalWrite(LED_BUILTIN, LOW);
    espInit();
  } else {
    Serial.println("No Double Reset Detected");
    digitalWrite(LED_BUILTIN, HIGH);
  }
  
   //amg.begin();
    mlx.begin();
    
   oled.begin(SSD1306_SWITCHCAPVCC, OLED_Address);
   
  // Clear the buffer
  oled.clearDisplay();
   
   mqttCh_c2s = mqttCh_c2s+m.HOSTNAME; 
   mqttCh_s2c = mqttCh_s2c+m.HOSTNAME;
    
   //display.clearDisplay();

   oled.setTextSize(1);
   oled.setTextColor(WHITE);
   oled.setCursor(20,30);
   
   oled.println("Ver: " + m.getVer());
   oled.display();
   delay(3000);
   oled.clearDisplay();

   oled.setCursor(30,30);
   oled.println(init_display);
   oled.display();
   delay(3000);
   oled.clearDisplay();
   
   m.wifiCallback(configModeCallback); 
   
    blinkInit();
   
   if (m.connectWifi("", "") == WL_CONNECTED) {
      oled.setCursor(15,30);
     oled.println("Connected to Wifi");
     oled.display();
   }else {
       oled.setCursor(10,25);
       oled.println("Connect to Wifi AP:");
       oled.setCursor(30,35);
       oled.println(m.HOSTNAME);
       oled.display();
       
      if (m.wifiConn(const_cast<char*>(m.HOSTNAME.c_str()))){
    
        delay(3000);
        oled.clearDisplay();
    
       oled.setCursor(15,30);
       oled.println("Connected to Wifi");
       oled.display();
       }
    }

   delay(3000);
   oled.clearDisplay();
   
   oled.setTextSize(1);
   oled.setCursor(10,30);
   oled.println(linking);
   oled.display();
   delay(2000);
   //display.clearDisplay();
   m.w(m.getMqttIP());
   if(m.getMqttIP() != ""){
    //m16.cloudmqtt.com
    mqttServer = m.getMqttIP();
   }
   if(m.getMqttPort() != 0){
    //11341
    mqttPort = m.getMqttPort();
   }
   if(m.getMqttUser() != ""){
    //qyvsypjo
    mqttUsr = m.getMqttUser();
   }
   
   if(m.getMqttPwd() != ""){
    //7MIVQcp49XLs
    mqttPwd = m.getMqttPwd();
   }
    
   client.setServer(mqttServer.c_str(), mqttPort);
   client.setCallback(mqttCallback); 
   
}


void loop() {
  
    float temperature = mlx.readObjectTempC();
    String temp;
    
    if (temperature < 110 && temperature > 0){
      // Write in EEPROM
      EEPROM.write(integer_address, (int)temperature);
      EEPROM.write(decimal_address, 100*(temperature - (int)temperature));
      EEPROM.commit();
      // Convert in string
        temp = String(temperature);
     }else{
        byte integer = EEPROM.read(integer_address);
        byte decimal = EEPROM.read(decimal_address);
        // Convert in string
        temp = String(integer+"."+decimal);
      
      }
   
    oled.clearDisplay();

    oled.setTextSize(1);
    oled.setCursor(1,0);
    oled.println("SSID: "+ m.getSSID());
    
    oled.setTextSize(2);
    oled.setCursor(20,30);
    //display.println(temp_ave+"*C");
    oled.println(temp+"*C");
    oled.display();
    //digitalWrite(D1, LOW);
    delay(1000);
    //display.clearDisplay();
    
    seconds = seconds + 1;
    
    if(seconds == 50){
      if (!client.connected()) {
        oled.clearDisplay();
        oled.setTextSize(1);
       oled.setCursor(2,25);
       oled.println("MQTT Connection failed:");
       oled.setCursor(30,35);
       oled.println("Reset module");
       oled.display();
        mqttReconnect();
      }
      mqttPub("readBuff","", temp);
      seconds = 0; 
     }
    client.loop();
    //drd.loop();
}


void blinkInit(){  
    lightUp(isLightUp);   
    delay(100);      
    lightUp(!isLightUp);   
    delay(100);
}

void lightUp(bool rs){ 
    pinMode(LED_BUILTIN , OUTPUT); 
    isLightUp = rs;
    digitalWrite(LED_BUILTIN,isLightUp); 
}

void espInit(){  
  ESP.eraseConfig();  
  m.wifiClr(); 
  ESP.restart();    
} 

void configModeCallback (WiFiManager *myWiFiManager) {  //gets called when WiFiManager enters configuration mode
  
}
void getVals(char* cmd) //取得該指令的値
{
  String rs = "";
  int cmdLen = strlen(cmd); 
  unsigned short x =  cal_crc_half((unsigned char*) cmd,cmdLen) ;  // 2 bytes 
  
  byte xh = byte((x & 0xFF00)>>8);
  byte xl = byte(x & 0x00FF);

  byte ca[cmdLen+3]; // size +3 for crc high,low bytes and /n
 
  for (int i=0;i<sizeof(ca);i++) 
    ca[i]=byte(13); 

  for (int i=0;i<cmdLen;i++) 
    ca[i]=(byte)cmd[i]; 
 
  ca[cmdLen]=xh;
  ca[cmdLen+1]=xl;
  ca[cmdLen+2]=byte(13);
  Serial.write(ca,sizeof(ca));   
  delay(1000); 
  Serial.flush();  
  //mqttPub("debug","","data flushed.");  
  delay(1000); 
  serReadBuff(cmd);
}  

void serReadBuff(String cmd)
{
  if(Serial.available()>0)
  {
    int i=0;
    while(Serial.available()>0)
    {
      byte a = Serial.read(); 
      if (a==byte(10) || a==byte(13)){}        
      else
        buff[i++]= (char)a; 
    } 
    mqttPub("readBuff",cmd,(String)buff);  
  }
  //else
  //  mqttPub("debug",(String)cmd,"Serial not available."); 
    
  memset(buff, 0, sizeof(buff));
}

void doCmd(String cmd)
{ 
   //m.w("do cmd ["+cmd+"]");
   cmd.trim();  
   getVals((char *)cmd.c_str());
   delay(1000);
}
void espReboot(){  
  ESP.restart();    
}
void doSysCmd(String cmd)
{ 
  //m.w("do Sys cmd ["+cmd+"]");
  if (cmd=="cBOOT") 
    espReboot(); 
 
  if (cmd=="cINIT") 
    espInit(); 
}

bool isSysCmd(String cmd)
{
  String sysCMD = "|cBOOT|cINIT|";
 
  if (sysCMD.indexOf("|"+cmd+"|")>=0)
    return true;
  else
    return false; 
}

//--------MQTT-------------
void mqttCallback(char* topic, byte* payload, unsigned int length)
{  // receive msg from mqtt broker. 
  String msg = "";
  for (int i = 0; i < length; i++) { 
    msg += (char)payload[i];    
  } 
  msg.trim();
  mqttPub("callback","",msg);      // send back to mqtt server.   
  if (isSysCmd(msg)) 
    doSysCmd(msg);                   // process commad from server.
  else
    doCmd(msg);
}

void mqttReconnect() 
{  // Loop until we're reconnected
  while (!client.connected()) {
    //Serial.print("Attempting MQTT connection..."); 
    String clientId =   m.HOSTNAME; // Create a random client ID, use hostname as mqtt client id.
 
    String lwMsg = clientId + " is disconnected.";
    
    if (client.connect(clientId.c_str(),mqttUsr.c_str(),mqttPwd.c_str(),mqttCh_LW.c_str(),mqttLWQoS,mqttLWRetain,lwMsg.c_str(),mqttClrSession)) {  // Attempt to connect
      mqttPub("connect","",m.getVer());       // Once connected,  an announcement...   
      client.subscribe(mqttCh_s2c.c_str());   // ... and resubscribe 
    } else {  
      delay(5000);  // Wait 5 seconds before retrying
    }
  }
}

void mqttPub(String fn,String cmd,String msg)
{
  /* ## PubSubClient.h > MQTT_MAX_PACKET_SIZE = 8912 */ 
  //msg.replace("(NAKss","");
  msg.trim(); 
  String data =  "";  
         data += "{";
         data += " \"f\":\""+fn+"\""; 
         data += ",\"c\":\""+cmd+"\"";
         data += ",\"h\":\""+m.HOSTNAME +"\"";    
         data += ",\"t\":\""+ msg +"\"";  //mqtt text 
         data += "}"; 
  Serial.println(mqttCh_c2s.c_str());
  
  client.publish(mqttCh_c2s.c_str(), data.c_str());    
}

unsigned  cal_crc_half(unsigned char *ptr, unsigned char len) 
{ 
    unsigned short crc;
    unsigned char da;
    unsigned char bCRCLow;
    unsigned char bCRCHign; 
    unsigned short crc_ta[16]={0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
    0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef};
    crc=0; 
    while(len--!=0)
    {
       da=((unsigned char)(crc>>8))>>4; 
       crc<<=4;  
       crc^=crc_ta[da^(*ptr>>4)];
       da=((unsigned char)(crc>>8))>>4; 
       crc<<=4;  
       crc^=crc_ta[da^(*ptr&0x0F)];
       ptr++; 
    }

    bCRCLow = (unsigned char)crc;
    bCRCHign= (unsigned char)(crc>>8);
 
    if(bCRCLow==0x28||bCRCLow==0x0d||bCRCLow==0x0a) 
      bCRCLow++;
    
    if(bCRCHign==0x28||bCRCHign==0x0d||bCRCHign==0x0a)
      bCRCHign++;
             
    crc = ((unsigned short)bCRCHign)<<8;
    crc += bCRCLow;
 
    return(crc);
}

//--------- LCD   -----@ 190502 by Kay-----------
void lcdShow (String msg,int rowNum,int padding_left,int tSize )
{ 
   oled.setTextSize(tSize);
   oled.setTextColor(WHITE); 

   int x = padding_left;
   int y = (rowNum-1)*11;
    
   oled.setCursor(x,y);
   oled.println(msg);
   oled.display();
}
void lcdClear()
{
    oled.clearDisplay();  
}